#!/bin/sh

# Read general test configuration
# ===============================

PROBLEM_XML="$PROBLEM_ROOT/Problem.xml"

PATCHER_EXECUTABLE_FILENAME="$(xmllint --nocdata --xpath 'string(/Problem/@PatcherExe)' "$PROBLEM_XML")"
PATCHER_EXECUTABLE_FILEPATH="$PROBLEM_ROOT/$PATCHER_EXECUTABLE_FILENAME"
