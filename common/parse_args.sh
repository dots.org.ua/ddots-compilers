#!/bin/sh

# Stop execution when a command fails
set -e

# Stop execution when undefined variable is used
set -u

SOURCE_FILEPATH=$1
EXECUTABLE_FILEPATH=$2
PROBLEM_ROOT=$3

if [ ! -f "$SOURCE_FILEPATH" ]; then
    echo "Source file does not exist" >&2
    exit 1
fi

if [ ! -d "$PROBLEM_ROOT" ]; then
    echo "Problem root folder does not exist" >&2
    exit 1
fi
