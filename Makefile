DOCKER_COMPILERS := $(notdir $(wildcard images/*))

BUILD_PATH = build-$@
COMMON_PATH = common

define get_last_update
	`stat -c %y $1 | sort -r | head -1 | sed 's:\..*$$::'`
endef

COMPILE_SH_PARTS = \
	$(COMMON_PATH)/parse_args.sh \
	$(COMMON_PATH)/global_config.sh \
	$(COMMON_PATH)/patch_solution.sh \
	images/$@/compile.sh
COMPILE_SH_PARTS_LAST_UPDATE = $(call get_last_update,$(COMPILE_SH_PARTS))
DOCKERFILE_PARTS = images/$@/Dockerfile.head $(COMMON_PATH)/Dockerfile.tail

# we target bash in echo statements, though it is not that critical
SHELL := bash

COLOR_GREEN := \033[1;32m
COLOR_WHITE := \033[1;37m
COLOR_NONE := \033[0m

define echo
	echo $$'$(COLOR_GREEN)'$1$$'$(COLOR_NONE)'
endef


.PHONY: all
all: $(DOCKER_COMPILERS)

.PHONY: $(DOCKER_COMPILERS)
$(DOCKER_COMPILERS):
	@$(call echo,$$'Building "$(COLOR_WHITE)$@$(COLOR_GREEN)" compiler image...')
	rm -rf $(BUILD_PATH)
	mkdir $(BUILD_PATH)
	
	cp -Lrp images/$@/* $(BUILD_PATH)/
	cat $(COMPILE_SH_PARTS) > $(BUILD_PATH)/compile.sh
	# Set a modification date to avoid unnecessary docker rebuilds
	touch -m -d "$(COMPILE_SH_PARTS_LAST_UPDATE)" $(BUILD_PATH)/compile.sh
	chmod 664 $(BUILD_PATH)/compile.sh
	
	cat $(DOCKERFILE_PARTS) > $(BUILD_PATH)/Dockerfile
	echo "ENV COMPILER_NAME='$@'" >> $(BUILD_PATH)/Dockerfile
	
	cd $(BUILD_PATH) && docker build --pull --tag ddots-compiler-$@ .
	rm -r $(BUILD_PATH)
	@$(call echo,$$'"$(COLOR_WHITE)$@$(COLOR_GREEN)" compiler image has been built')
