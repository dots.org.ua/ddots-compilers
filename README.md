Compilers Docker images for DDOTS
=================================

This repo contains Docker images with compilers for DDOTS.

There are the following compilers right now:

* gcc (C compiler)
* g++ (C++ compiler)
* gcc c11 mode (C compiler)
* g++ c++11 mode (C++ compiler)
* fpc (FreePascal compiler)
* fpc in Delphi mode
* java (openjdk or oracle?)
* go lang
* C# (mono)

These are going to be implemented:

* nuitka (Python code compiler)


HowTo build images
------------------

Makefile is prepared and it is very easy to rebuild all images:

```bash
$ make
```

If you want ot rebuild only one image:

```bash
$ make fpc
```
