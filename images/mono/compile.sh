#!/bin/sh

compilation_exit_code=0
compilation_message="$(mcs -warn:0 -r:System.Numerics -optimize+ "-out:$EXECUTABLE_FILEPATH" "$SOURCE_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | sed "s:$SOURCE_FILEPATH:solution.source:"
fi
exit "$compilation_exit_code"
