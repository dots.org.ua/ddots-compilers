#!/bin/sh

if ! grep -qiE '<\?php([^a-zA-Z0-9_\-]|$)' "$SOURCE_FILEPATH"
then
    echo "solution.source: Testing System Compilation Error: There must be a \`<?php\` tag"
    exit 1
fi

syntax_check_exit_code=0
syntax_check_message="$(php -l "$SOURCE_FILEPATH" 2>&1)" || \
    syntax_check_exit_code="$?"

if [ "$syntax_check_exit_code" -ne 0 ]
then
    echo "$syntax_check_message" | sed "s:$SOURCE_FILEPATH:solution.source:"
else
    cp "$SOURCE_FILEPATH" "$EXECUTABLE_FILEPATH"
fi
exit "$syntax_check_exit_code"
