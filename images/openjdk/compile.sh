#!/bin/sh

if ! grep -qE 'class +Main([^a-zA-Z0-9_\-]|$)' "$SOURCE_FILEPATH"
then
    echo "solution.source: Testing System Compilation Error: There must be a class named \`Main\`"
    exit 2
fi

if ! grep -qE 'void +main([^a-zA-Z0-9_\-]|$)' "$SOURCE_FILEPATH"
then
    echo "solution.source: Testing System Compilation Error: There must be a method named \`main\`"
    exit 2
fi

if grep -qE '^ *package +[a-zA-Z0-9]*;' "$SOURCE_FILEPATH"
then
    echo "solution.source: Testing System Compilation Error: Your solution must not be a package (remove \`package\` directive)"
    exit 2
fi

# Java requires to have a specific filename, so we have to rename solution
# before compilation
cd "$(dirname "$EXECUTABLE_FILEPATH")"
SOURCE_JAVA_FILEPATH="Main.java"
cp "$SOURCE_FILEPATH" "$SOURCE_JAVA_FILEPATH"

compilation_exit_code=0
compilation_message="$(javac -nowarn -g:none Main.java 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | grep -v 'Note: ' | sed "s:$SOURCE_JAVA_FILEPATH:solution.source:"
    exit "$compilation_exit_code"
fi

if [ ! -f "Main.class" ]
then
    echo "solution.source: Testing System Compilation Error: There must be a class named \`Main\`"
    exit 2
fi

echo "Main-Class: Main" > MANIFEST.MF

if ! jar -c0fm "$EXECUTABLE_FILEPATH" MANIFEST.MF *.class
then
    echo "solution.source: Testing System Compilation Error: Couldn't create a jar file"
    exit 3
fi

exit 0
