#!/bin/sh

compilation_exit_code=0
compilation_message="$(node -c "$SOURCE_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"
if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | sed "s:$SOURCE_FILEPATH:solution.source:"
fi

cp "$SOURCE_FILEPATH" "$EXECUTABLE_FILEPATH"
exit "$compilation_exit_code"
