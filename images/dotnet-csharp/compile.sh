#!/bin/sh

cd "$(dirname "$SOURCE_FILEPATH")"
mv "$SOURCE_FILEPATH" "./solution.cs"
cat >solution.csproj <<EOF
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp7.0</TargetFramework>
  </PropertyGroup>

</Project>
EOF

mkdir -p obj
cat >./obj/project.assets.json <<EOF
{
  "version": 3,
  "targets": {
    "net7.0": {}
  },
  "libraries": {},
  "projectFileDependencyGroups": {
    "net7.0": []
  },
  "packageFolders": {
    "/root/.nuget/packages/": {}
  },
  "project": {
    "version": "1.0.0",
    "restore": {
      "packagesPath": "/root/.nuget/packages/",
      "projectStyle": "PackageReference",
      "configFilePaths": [
        "/root/.nuget/NuGet/NuGet.Config"
      ],
      "originalTargetFrameworks": [
        "netcoreapp7.0"
      ],
      "sources": {
        "https://api.nuget.org/v3/index.json": {}
      }
    }
  }
}
EOF

# Without this file you will get the following compilation error:
#
# System.IO.IOException: The system cannot open the device or file specified. : 'NuGet-Migrations'
mkdir -p .local/share/NuGet/Migrations/
touch .local/share/NuGet/Migrations/1

compilation_exit_code=0
compilation_message="$(HOME="$(dirname "$EXECUTABLE_FILEPATH")" DOTNET_NOLOGO=1 DOTNET_CLI_TELEMETRY_OPTOUT=1 dotnet build --configuration Release --output ./build --verbosity quiet --nologo --no-restore solution.csproj 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | sed "s:$(dirname "$SOURCE_FILEPATH")/::g;s:solution.csproj:solution.source:g;s:solution.cs:solution.source:g"
    exit "$compilation_exit_code"
fi

mv "./build/solution.dll" "$EXECUTABLE_FILEPATH"

exit 0
