#!/bin/sh

compilation_exit_code=0
compilation_message="$(nim compile --define:release --verbosity:0 --nimcache:"$(dirname "$EXECUTABLE_FILEPATH")" --out:"$EXECUTABLE_FILEPATH" "$SOURCE_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message"
fi
exit "$compilation_exit_code"
