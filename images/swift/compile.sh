#!/bin/sh
set -x -e
# Swift requires to have a specific file extension, so we have to rename a solution
# before compilation
SOURCE_SWIFT_FILEPATH="$EXECUTABLE_FILEPATH.swift"
cp "$SOURCE_FILEPATH" "$SOURCE_SWIFT_FILEPATH"

compilation_exit_code=0
export TMPDIR="$(dirname "$EXECUTABLE_FILEPATH")"
compilation_message="$(swiftc -O -module-cache-path "$TMPDIR" -o "$EXECUTABLE_FILEPATH" "$SOURCE_SWIFT_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | sed "s:$SOURCE_SWIFT_FILEPATH:solution.source:"
fi
exit "$compilation_exit_code"
