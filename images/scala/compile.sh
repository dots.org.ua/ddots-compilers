#!/bin/sh

cd "$(dirname "$EXECUTABLE_FILEPATH")"
compilation_exit_code=0
compilation_message="$(scalac -nowarn -g:none "$SOURCE_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | grep -v 'Note: ' | sed 's:Main.java:solution.source:'
    exit "$compilation_exit_code"
fi

if [ ! -f "Main.class" ]
then
    echo "solution.source: Testing System Compilation Error: There must be a class named \`Main\`"
    exit 2
fi

cat > MANIFEST.MF <<EOF
Main-Class: Main
Class-Path: ${SCALA_HOME}/lib/scala-library.jar
EOF

if ! jar -c0fm "$EXECUTABLE_FILEPATH" MANIFEST.MF ./*.class
then
    echo "solution.source: Testing System Compilation Error: Couldn't create a jar file"
    exit 3
fi

exit 0
