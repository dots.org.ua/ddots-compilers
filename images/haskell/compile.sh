#!/bin/sh

# Haskell requires to have a specific file extension, so we have to rename a solution
# before compilation
SOURCE_HASKELL_FILEPATH="$EXECUTABLE_FILEPATH.hs"
cp "$SOURCE_FILEPATH" "$SOURCE_HASKELL_FILEPATH"

compilation_exit_code=0
compilation_message="$(TMPDIR="$(dirname "$EXECUTABLE_FILEPATH")" ghc -O2 -static -optl-static "$SOURCE_HASKELL_FILEPATH" -o "$EXECUTABLE_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | sed "s:$SOURCE_HASKELL_FILEPATH:solution.source:"
fi
exit "$compilation_exit_code"
