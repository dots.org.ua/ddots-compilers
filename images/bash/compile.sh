#!/bin/sh

syntax_check_exit_code=0
syntax_check_message="$(bash -n "$SOURCE_FILEPATH" 2>&1)" || \
    syntax_check_exit_code="$?"

if [ "$syntax_check_exit_code" -ne 0 ]
then
    echo "$syntax_check_message" | sed "s:$SOURCE_FILEPATH:solution.source:"
else
    cp "$SOURCE_FILEPATH" "$EXECUTABLE_FILEPATH"
fi
exit "$syntax_check_exit_code"
