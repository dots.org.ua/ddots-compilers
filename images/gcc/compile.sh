#!/bin/sh

source "$(dirname "$0")/compiler_extra_options.sh"

compilation_exit_code=0
compilation_message="$(gcc -std="$COMPILER_DIALECT" -w -O2 --static -x c "$SOURCE_FILEPATH" -o "$EXECUTABLE_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | sed "s:$SOURCE_FILEPATH:solution.source:"
fi
exit "$compilation_exit_code"
