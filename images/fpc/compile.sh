#!/bin/sh

source "$(dirname "$0")/compiler_extra_options.sh"

compilation_exit_code=0
compilation_message="$(fpc -M"$COMPILER_DIALECT" -v0 -O2 -XS -Xt -Xs "$SOURCE_FILEPATH" -o"$EXECUTABLE_FILEPATH" 2>/dev/null)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message"
fi
exit "$compilation_exit_code"
