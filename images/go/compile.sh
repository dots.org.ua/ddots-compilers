#!/bin/sh

# Go requires to have a specific file extension, so we have to rename a solution
# before compilation
SOURCE_GO_FILEPATH="$EXECUTABLE_FILEPATH.go"
cp "$SOURCE_FILEPATH" "$SOURCE_GO_FILEPATH"

compilation_exit_code=0
compilation_message="$(TMPDIR="$(dirname "$EXECUTABLE_FILEPATH")" HOME="$(dirname "$EXECUTABLE_FILEPATH")" go build -o "$EXECUTABLE_FILEPATH" "$SOURCE_GO_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | sed "s:${SOURCE_GO_FILEPATH:1}:solution.source:"
fi
exit "$compilation_exit_code"
