#!/bin/sh

compilation_exit_code=0
compilation_message="$(pypy3 -c "import py_compile
try:
    py_compile.compile(file='$SOURCE_FILEPATH', cfile='$EXECUTABLE_FILEPATH', dfile='solution.source', doraise=True)
except py_compile.PyCompileError as compilation_error:
    print(compilation_error.msg)
    import sys
    sys.exit(1)
")" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | sed "s:$SOURCE_FILEPATH:solution.source:"
fi

cp "$SOURCE_FILEPATH" "$EXECUTABLE_FILEPATH"

exit "$compilation_exit_code"
