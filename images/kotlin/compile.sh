#!/bin/sh

if ! grep -qE 'fun +main([^a-zA-Z0-9_\-]|$)' "$SOURCE_FILEPATH"
then
    echo "solution.source: Testing System Compilation Error: There must be a function named \`main\`"
    exit 2
fi

# Kotlin requires a solution file name to have `.kt` file extension, so we have
# to rename a solution file before a compilation.
SOURCE_KOTLIN_FILEPATH="$EXECUTABLE_FILEPATH.kt"
cp "$SOURCE_FILEPATH" "$SOURCE_KOTLIN_FILEPATH"

compilation_exit_code=0
compilation_message="$(kotlinc -Djava.io.tmpdir="$(dirname "$EXECUTABLE_FILEPATH")" -nowarn -include-runtime -d "$EXECUTABLE_FILEPATH.jar" "$SOURCE_KOTLIN_FILEPATH" 2>&1)" || \
    compilation_exit_code="$?"

if [ "$compilation_exit_code" -ne 0 ]
then
    echo "$compilation_message" | grep -v 'Note: ' | sed "s:$SOURCE_KOTLIN_FILEPATH:solution.source:"
    exit "$compilation_exit_code"
fi

mv "$EXECUTABLE_FILEPATH.jar" "$EXECUTABLE_FILEPATH"

exit 0
